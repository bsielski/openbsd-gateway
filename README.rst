TODO
====
* Return ICMP 'Packet Too Big' when appropriate
* Setup a VPN
* Experiment with alternatives to OpenBSD (since NAT64 seems to be
  more of a pain than a benefit, NixOS perhaps)
